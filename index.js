var searchButton = document.getElementById("schbtn");
var loginButton = document.getElementById("loginbtn");
var regisButton = document.getElementById("regisbtn");
var addButton = document.getElementById("addbtn");
var deleteButton = document.getElementById("delbtn");
var xhttp = new XMLHttpRequest();

var vaild = 0;//flag variable for login vaildation

//4 input column
var codeinfo;
var nameinfo;
var emailinfo;
var pwinfo;
  
//event listener of 5 button  
loginButton.addEventListener('click', doLogin, false);
regisButton.addEventListener('click', doRegis, false);
addButton.addEventListener('click', doAdd, false);
searchButton.addEventListener('click', doSearch, false);
deleteButton.addEventListener('click', doDelete, false);

//login function: related to GET function: get the users information for login in server.js
function doLogin() {
    emailinfo = document.getElementById("email");
    pwinfo = document.getElementById("password");
    
    //check whether email or password are empty
    if(emailinfo == null){
        alert("please enter email");
    }else if(pwinfo == null){
        alert("please enter password");
    }else{
        xhttp.open("GET","https://restfullab-vtc177100259.c9users.io/users/email="
                    + emailinfo + "&password=" + pwinfo, true);
    }
    
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    var response = JSON.parse(xhttp.response);
    console.log(response);
    if(response != null){
        vaild += 1;             //change the flag status to 1 which mean vaild
    }else{
        vaild = 0;
    }
}
//register function: related to POST function: add a new user into user database in server.js
function doRegis() {
    emailinfo = document.getElementById("email");
    pwinfo = document.getElementById("password");
    
    //check whether email or password are empty
    if(emailinfo == null){
        alert("please enter email");
    }else if(pwinfo == null){
        alert("please enter password");
    }else{
        xhttp.open("POST","https://restfullab-vtc177100259.c9users.io/users/email="
                    + emailinfo + "&password=" + pwinfo, true);
    }
    
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    var response = JSON.parse(xhttp.response);
    console.log(response);
}

//search function:related to GET function: get the items information in server.js
function doSearch() {
    codeinfo = document.getElementById("code");
    nameinfo = document.getElementById("name");
    
    //check whether code or name are empty for searching reference
    if(codeinfo == null){
        xhttp.open("GET","https://restfullab-vtc177100259.c9users.io/items/name="
                    + nameinfo, true);
    }else if(nameinfo == null){
        xhttp.open("GET","https://restfullab-vtc177100259.c9users.io/items/code="
                    + codeinfo, true);
    }else{
        xhttp.open("GET","https://restfullab-vtc177100259.c9users.io/items/code="
                    + codeinfo + "&name=" + nameinfo, true);
    }
    
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    var response = JSON.parse(xhttp.response);
    console.log(response);
}

//function below should be logined fisrt before use
//add function: related to POST function: add a new item into item database in server.js
function doAdd() {
    //check whether the user login yet
    if(vaild == 1){         //if user has log-ined
        xhttp.open("POST","https://restfullab-vtc177100259.c9users.io/items", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    var response = JSON.parse(xhttp.response);
    console.log(response);
    }else{                  //if the user havenot login
        alert('please login first!');
    }
    
}

//delete function: related to DELETE function: delete existing item data in server.js
function doDelete() {
    //check whether the user login yet
    if(vaild == 1){         //if user has log-ined
        xhttp.open("DELETE","https://restfullab-vtc177100259.c9users.io/items", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    var response = JSON.parse(xhttp.response);
    console.log(response);
    }else{                  //if the user havenot login
        alert('please login first!');
    }
	
}